let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    if (collection.length === 0) {
        return collection;
    }

    var newCollection = [];

    for (var i = 1; i < collection.length; i++) {
        newCollection[i - 1] = collection[i];
    }

    collection = newCollection;
    return collection;
}

function front() {
    if (collection.length === 0) {
        return undefined;
    }
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};

